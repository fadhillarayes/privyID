// import Navbar from './Component/Navbar';
import Sidebar from './Component/Sidebar';
import Otp from './Pages/Otp';
// import Login from './Pages/Login';
// import Regis from './Pages/Regis';

function App() {
  return (
    <>
    <Sidebar />
    <Otp />
    </>
  );
}

export default App;

import React from 'react'
import '../Assets/Styles/Login.scss'

function Login() {
    return (
        <div>
            <div className="login__container col-lg-8">
                <div className="login__wrapper">
                    <div className="login__title">
                        <h2>Login Account</h2>
                    </div>
                    <div className="login__number">
                        <h5>Phone number</h5>
                        <input type="number" placeholder="+62xxxx"/>
                    </div>
                    <div className="login__password">
                        <h5>Password</h5>
                        <input placeholder="Enter your password" />
                    </div>
                </div>
                    <div className="login__button">
                        <button>Reset</button>
                        <button>Login</button>
                    </div>
            </div>
        </div>
    )
}

export default Login

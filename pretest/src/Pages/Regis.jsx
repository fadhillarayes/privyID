import React, {useState} from 'react';
import '../Assets/Styles/Regis.scss';
import Dropdown from '../Component/Dropdown';
import countries from '../Data/countries.json';
import '../Assets/Styles/Dropdown.css';
import terms from '../Assets/Images/terms.png';

function Regis () {
  const [value, setValue] = useState (null);
  return (
    <div>
      <div className="regis__container col-lg-8">
        <div className="regis__wrapper">
          <div className="regis__title">
            <h2>Create New Account</h2>
            <p>Before you can invest here, please create new account</p>
          </div>
          <div className="regis__detail">
            <h2>Account Detail</h2>
            <h5>Select Country</h5>
            <div style={{width: 200}}>
              <Dropdown
                options={countries}
                value={value}
                onChange={val => setValue (val)}
                prompt="Select country..."
              />
            </div>
          </div>
          <div className="regis__number">
            <h5>Phone number</h5>
            <input type="number" placeholder="+62xxxx" />
          </div>
          <div className="regis__password">
            <h5>Password</h5>
            <input placeholder="Enter your password" />
          </div>
        </div>
        <div className="regis__terms">
          <img src={terms} alt="" />
          <p>Terms and Condition</p>
        </div>
        <div className="regis__button">
          <button>Reset</button>
          <button>Register</button>
        </div>
      </div>
    </div>
  );
}

export default Regis;

import React from 'react';
import '../Assets/Styles/Otp.scss';
import resend from '../Assets/Images/resend.png';

function Otp () {
  return (
    <div>
      <div className="otp__container col-lg-8 align-self-end">
        <div className="otp__date">
          <p>Today Oct 11, 2021</p>
        </div>
        <div className="otp__wrapper">
          <div className="otp__title">
            <h1>OTP Verification</h1>
            <p>Insert OTP code sent to your phone</p>
          </div>
          <div className="otp__code">
            <input />
            <input />
            <input />
            <input />
            <button>Verify</button>
          </div>
        </div>
        <div className="otp__resend">
          <img src={resend} alt="" />
          <p>Resend OTP Code</p>
        </div>
      </div>
    </div>
  );
}

export default Otp;

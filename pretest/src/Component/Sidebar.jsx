import React from 'react';
import logo from "../Assets/Images/logo.png";
import "../Assets/Styles/Sidebar.scss"

function Sidebar() {
    return (
        <div>
            <div className="sidebar__container col-lg-4">
                <div className="sidebar__wrapper">
                    <div className="sidebar__title">
                        <img src={logo} alt="" />
                        <p>COINPRIVY</p>
                    </div>
                    <div className="sidebar__content">
                        <h1>Welcome to Coinprivy</h1>
                        <p>is a secure platform that makes it easy to buy, sell, and store cryptocurrency like Bitcoin, Ethereum, and more. Based in the USA</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Sidebar

import React from 'react'
import '../Assets/Styles/Navbar.scss'

function Navbar() {
    return (
        <div className="navbar__container">
        <div className="navbar__date ">
            <h5>Today, 8 Oct 2021</h5>
        </div>
        <div className="navbar__option">
            <h2>Login</h2>
            <h2>Registration</h2>
        </div>
        </div>
    )
}

export default Navbar
